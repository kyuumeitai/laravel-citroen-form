<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LeadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required | max:80',
            'carrera' => 'required | max:80',
            'email' => 'required | email | max:80 ',
            'ip' => 'max:100'
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Se necesita el nombre',
            'carrera.required' => 'Se necesita la carrera',
            'email.required' => 'Se necesita el email'
        ];
    }
}
