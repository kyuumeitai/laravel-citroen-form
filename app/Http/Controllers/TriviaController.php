<?php

namespace App\Http\Controllers;

use App\Trivia;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\TriviaRequest;
use Mail;

class TriviaController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TriviaRequest $request)
    {
        $trivia = new Trivia($request->all());
        $trivia->save();
        Mail::send('emails/notifytrivia',
        array(
            'email' => $request->get('email')
        ), function($message)
        {
            $message->from('dev.lacomarca@gmail.com', 'Dev La Comarca');
            $message->to('dev.lacomarca@gmail.com')->subject('Trivia Peugeot');
        }
    );
        return response()->json(['status' => 'OK']);
    }
}
