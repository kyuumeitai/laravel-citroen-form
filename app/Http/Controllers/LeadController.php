<?php

namespace App\Http\Controllers;

use App\Lead;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\LeadRequest;
use Mail;

class LeadController extends Controller
{

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LeadRequest $request)
    {
        $lead = new Lead($request->all());
        $lead->save();
        Mail::send('emails/notifycontact',
        array(
            'name' => $request->get('name'),
            'carrera' => $request->get('carrera'),
            'email' => $request->get('email'),
            'ip' => $request->ip()
        ), function($message)
        {

            $message->from('dev.lunamedia@gmail.com', 'Dev');
            $message->to(['dev.lunamedia@gmail.com','nicolas.diaz@grupocopesa.cl', 'juan.herrera@grupocopesa.cl', 'sebastian.abarca@grupocopesa.cl', 'camila.delarivera@grupocopesa.cl', 'alex.acuna@grupocopesa.cl'])->subject('Banner Utem Contacto');
        }
    );
        return response()->json(['status' => 'OK']);
    }

}
